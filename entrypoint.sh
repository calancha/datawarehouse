#! /bin/bash
export CELERY_FILESYSTEM_PATH="${CELERY_FILESYSTEM_PATH:-$(mktemp -d -t dw-celery.XXXXX)}"
export LOGS_PATH="${LOGS_PATH:-$(mktemp -d -t dw-logs.XXXXX)}"

export PROMETHEUS_MULTIPROC_DIR="/tmp/django_metrics"
rm -rf "${PROMETHEUS_MULTIPROC_DIR}"
mkdir -p "${PROMETHEUS_MULTIPROC_DIR}"

python3 manage.py migrate
environment=$(cki_deployment_environment)
# is_production_or_staging()
if [[ ${environment} =~ ^(production|staging) ]]; then
    python3 manage.py collectstatic
    supervisord
    tail --follow=name --retry /logs/nginx-{access,error}.log
else
    python3 -m pip install -e .
    supervisord -c /code/configs/supervisor-app.devel.ini
fi
