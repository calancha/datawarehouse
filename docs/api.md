# REST API

## Pagination

Requests that return multiple items will be paginated by 30 items by default.
You can change this using the ?limit and ?offset parameters.
You can also set custom page sizes up to 100 using the ?limit parameter.

More information is available in the
[LimitOffsetPagination docs](https://www.django-rest-framework.org/api-guide/pagination/#limitoffsetpagination).
