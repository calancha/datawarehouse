"""Cron tasks to update prometheus metrics."""
from cki_lib.logger import get_logger

from datawarehouse import metrics
from datawarehouse.cron.utils import CronJobWithDBConnection

LOGGER = get_logger(__name__)


class UpdatePrometheusMetrics5m(CronJobWithDBConnection):
    """Update prometheus metrics every 5 minutes."""

    schedule = '*/5 * * * *'

    def entrypoint(self, **_):
        """Run job."""
        metrics.update_unfinished_builds()
        metrics.update_unfinished_tests()


class UpdatePrometheusMetrics1h(CronJobWithDBConnection):
    """Update prometheus metrics every 1 hour."""

    schedule = '0 * * * *'

    def entrypoint(self, **_):
        """Run job."""
        metrics.update_issues()
        metrics.update_baselines()
