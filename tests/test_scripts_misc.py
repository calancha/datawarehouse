"""Test the scripts.misc module."""
from unittest import mock

from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.scripts import misc
from tests import utils


class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""
    fixtures = [
        'tests/fixtures/scripts_misc.yaml',
    ]

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_objects_for_retriage(self, send_notif):
        """Test send_kcidb_object_for_retriage."""

        checkout_1 = models.KCIDBCheckout.objects.get(id='rev1')

        issue_regex_1 = models.IssueRegex.objects.get(pk=1)
        issue_regex_2 = models.IssueRegex.objects.get(pk=2)

        build_1 = models.KCIDBBuild.objects.get(id='redhat:build-1')

        test_1 = models.KCIDBTest.objects.get(id='redhat:test-1')
        test_2 = models.KCIDBTest.objects.get(id='redhat:test-2')

        testresult_4 = models.KCIDBTestResult.objects.get(id='redhat:test-4.1')

        misc.send_kcidb_object_for_retriage([
            {'since_days_ago': 3, 'issueregex_id': 999},  # Missing id
        ])
        self.assertFalse(send_notif.called)

        misc.send_kcidb_object_for_retriage([
            {'since_days_ago': 3, 'issueregex_id': issue_regex_1.id},
            {'since_days_ago': 3, 'issueregex_id': issue_regex_2.id},
            {'since_days_ago': 3, 'issueregex_id': issue_regex_1.id},
            {'since_days_ago': 3, 'issueregex_id': 999},  # Missing id
        ])
        send_notif.assert_has_calls(
            [
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'checkout',
                    'object': serializers.KCIDBCheckoutSerializer(checkout_1).data,
                    'id': checkout_1.id,
                    'iid': checkout_1.iid,
                    'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'build',
                    'object': serializers.KCIDBBuildSerializer(build_1).data,
                    'id': build_1.id,
                    'iid': build_1.iid,
                    'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                }]),
                mock.call([
                    {
                        'timestamp': '2010-01-02T09:00:00+00:00',
                        'status': 'needs_triage',
                        'object_type': 'test',
                        'object': serializers.KCIDBTestSerializer(test_1).data,
                        'id': test_1.id,
                        'iid': test_1.iid,
                        'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                    },
                    {
                        'timestamp': '2010-01-02T09:00:00+00:00',
                        'status': 'needs_triage',
                        'object_type': 'test',
                        'object': serializers.KCIDBTestSerializer(test_2).data,
                        'id': test_2.id,
                        'iid': test_2.iid,
                        'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                    }
                ]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'testresult',
                    'object': serializers.KCIDBTestResultSerializer(testresult_4).data,
                    'id': testresult_4.id,
                    'iid': testresult_4.iid,
                    'misc': {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]},
                }]),
            ]
        )
